<?php
/**
 * Created By: katherinedepadua
 * Company: Yondu Inc.
 * Department: MS - Platform
 * Date: 10/19/2015
 * Purpose: Daiy Dose API
 */
error_reporting(E_ALL);
require 'ddose.class.php';

class profile extends ddose
{
	public $dir = '/var/www/html/daily_dose/public/assets/img/profile_pic/';
	public $path = 'http://23.253.177.140/daily_dose/public/assets/img/profile_pic/';
	public $kb = 5242880; //in bytes 5mb
	
	public function __construct($file,$data){
		parent::__construct();

		$this->upload($file,$data);
	}

	public function upload($file,$data){

		$temp = explode(".", $file["image"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);

		$uploadfile = $this->dir . $newfilename;

		//$details = getimagesize($file['image']['name']);
		//print_r($file);
		//var_dump($details);
		$file_tmp = $file['image']['tmp_name'];
		$size = filesize($file_tmp); //var_dump($size);


		if($file['image']['size'] < $this->kb){
			
			if (move_uploaded_file($file['image']['tmp_name'], $uploadfile)) {

				$dataset = array('image_url' => $this->path.$newfilename,
							'msisdn' => $data['msisdn']
						);

				$update = $this->update_registration_details($dataset);

				//var_dump($update);

		    	$msg = array('status' => "success", 'message' => 'your profile uploaded successfully ', 'image_url' => $this->path.$newfilename);
			} else {
			    $msg = array('status' => "failed", 'message' => $file['image']['error']);
			}

		}else{
			$msg = array('status' => "failed", 'message' => 'maximum size exceeded');
		}

		echo json_encode($msg);
	}

	public function __destruct(){
		parent::__destruct();
	}
}

//var_dump($_POST);
new profile($_FILES,$_POST);