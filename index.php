<?php
/**
 * Created By: katherinedepadua
 * Company: Yondu Inc.
 * Department: MS - Platform
 * Date: 09/07/2015
 * Purpose: Daiy Dose API
 */
error_reporting(E_ALL);
require 'ddose.class.php';
require 'validation.php';

class app extends ddose{

public $data = array();
private $validation;
private $err;
private $assets;
private $version = '1.5'; //Updated version code to 1.5 (for mandatory update) - CSC20160427

public function __construct($data){

	parent::__construct();

	$this->assets = 'http://23.253.177.140/daily_dose/public';
	$this->data = $data;
	$this->api();
}

public function api(){
	//var_dump($this->data['method']);

	switch ($this->data['method']) {
		case 'register':
			$this->register();
			break;
		case 'profile':
			$this->profile();
			break;
		case 'credentials':
			$this->credentials();
			break;			
		case 'mobile_login':
			$this->mobile_login();
			break;
		case 'fb_login':
			$this->fb_login();
			break;
		case 'send_verif':
			$this->send_verification();
			break;
		case 'check_verif':
			$this->check_verification();
			break;
		case 'reset':
			$this->reset_password();
			break;
		case 'update_gcm':
			$this->update_gcm();
			break;
		case 'categories':
			$this->content_categories();
			break;
		case 'content':
			$this->content();
			break;
		case 'activity':
			$this->subs_activity();
			break;
		case 'is_read':
			$this->is_read();
			break;
		case 'is_deleted':
			$this->is_deleted();
			break;
		case 'is_favorite':
			$this->is_favorite();
			break;
		case 'push_charged': //push api
			$this->push_charged();
			break;
		case 'edit_profile':
			$this->edit_profile();
			break;
		case 'optin':
			$this->optin();
			break;
		case 'check_subscriber': //push api
			$this->check_subscriber();
			break;
		case 'log':
			$this->log();
			break;
		case 'badge':
			$this->badge();
			break;
		case 'subscription_type':
			$this->subscription_type();
			break;
		case 'freetrial':
			$this->freetrial();
			break;
		case 'status_id':
			$this->status_id();
			break;
		case 'subscriber_update':
			$this->subscriber_update();
			break;
		case 'version':
			$this->version();
			break;
		case 'session':
			$this->session();
			break;
		case 'user_session':
			$this->user_session();
			break; 
		default:
			echo 'Invalid Parameter';
			break;
	}
}

/*
* User Registration
* Param Required: 
*/
public function register(){

	$this->validation = new validation();

	$this->validation->field_name('Full Name')->field_value($this->data['fullname'])->required();
	$this->validation->field_name('Mobile Number')->field_value($this->data['msisdn'])->is_mobile_number()->is_globe()->required();
	$this->validation->field_name('Password')->field_value($this->data['password'])->required();

	if (!$this->validation->err) {
		$dataset = $this->get_subscriber($this->data['msisdn']);
		
		if(count($dataset)){
			$this->validation->err['status'] = 'failed';
			$this->validation->err['message'] = 'You are already registered';
		}else{
			$data = array('facebook_id' => $this->data['facebook_id'],
						  'msisdn' => $this->data['msisdn'],
						  'fullname' => $this->data['fullname'],
						  'password' => $this->data['password'],
						  'email' => $this->data['email'],
						  'birthday' => $this->data['birthday'],
						  'image_url' => $this->data['image_url'],
						  'gcm_id' => $this->data['gcm']);
			$id = $this->register_subscriber($data);
			if($id){
				$this->validation->err['status'] = 'success';
				$this->validation->err['message'] = 'You have successfully registered';

				$this->insert_initial_content($id);

			}	
		}
		

	}else{
		$this->validation->err['status'] = 'failed';
	}

	echo json_encode($this->validation->err);
}

public function profile(){
	$dataset = $this->get_subscriber($this->data['msisdn']);

	//$datetime1 = date_create(date('Y-m-d',strtotime($dataset['registered_at'])));
	//$datetime2 = date_create(date('Y-m-d'));
	//$interval = date_diff($datetime1, $datetime2); 

	//$free = $interval->days < 15 ? 1 : 0;
	//$dataset['daysLeftOfFreeTrial'] = $free;

	echo json_encode($dataset);
}
public function credentials(){
	$dataset = $this->get_credentials($this->data['msisdn']);
	echo json_encode($dataset);
}
public function mobile_login(){
	$dataset = $this->get_mobile_login($this->data['user_id'],$this->data['password']);
	
	if(count($dataset)){
		$data = array('status' => "success", 'message' => "Successfully logged in");
	}else{
		$data = array('status' => "failed", 'message' => "Invalid user and password combination");
	}

	echo json_encode($data);

}

public function fb_login(){
	$dataset = $this->get_fb_login($this->data['user_id']); 
	
	if(count($dataset)){
		$data = array('status' => "success", 'message' => $dataset['msisdn']);
	}else{
		$data = array('status' => "failed", 'message' => "Not a user");
	}

	echo json_encode($data);
}

public function send_verification(){
	$code = $this->randomString(1,'alpha').$this->randomString(3,'num'); 
	
	$dataset = array('msisdn' => $this->data['msisdn'],
					 'verification_code' => $code);

	$already_exist = $this->check_msisdn_verification($this->data['msisdn']);

	if(count($already_exist)){
		$this->update_verification_code($this->data['msisdn'],$code);
	}else{
		$this->insert_verification_code($dataset);
	}

	$message = "DAILY DOSE: Your verification code is ".$code.". If you haven't downloaded the app yet, you can get it at https://play.google.com/store/apps/details?id=com.yondu.dailydose, Data charges may apply. Help? Call 02-8873973, M-F. 8am-5pm This message is free.";

	$sms = array('msisdn' => $this->data['msisdn'],
				 'message' => urlencode($message));
	$this->send_sms($sms);

	$data = array('status' => "success", 'message' => "Verification Code sent");
	echo json_encode($data);
}


public function check_verification(){
	$verify = $this->check_verification_code($this->data['msisdn'], $this->data['code']);
	
	if(count($verify)){
		$data = array('status' => "success", 'message' => "Verification code accepted");
		$this->update_verified_at($this->data['msisdn']);
	}else{
		$data = array('status' => "failed", 'message' => "Invalid verification code");
	}
	echo json_encode($data);
}

public function reset_password(){
	$dataset = $this->get_subscriber($this->data['msisdn']);
	
	if(count($dataset)){
		$new_password = $this->randomString(6);

		$sms = array('msisdn' => $this->data['msisdn'],
					 'message' => urlencode("Your new password: ".$new_password));
		$this->send_sms($sms);


		$data = array('status' => "success", 'message' => "New password successfully sent");
		$this->update_password($this->data['msisdn'], $new_password);
	}else{
		$data = array('status' => "failed", 'message' => "You are not a subscriber");
	}

	echo json_encode($data);
}

public function update_gcm(){
	$this->update_gcm_id($this->data['msisdn'], $this->data['gcm']);
	$data = array('status' => "success");
	echo json_encode($data);
}

public function content_categories(){
	$data = $this->get_content_categories();

	while($row = $data->fetch_assoc()){
		$row['icon_url'] = $this->assets.$row['icon_url'];
		$dataset[] = $row;
	}

	echo json_encode($dataset);
}

public function content(){
	/*
	freetrial 
	0-expired (free content only)
	>0 - free trial (free and paid)
	-1 - subscribed (free and paid)
	*/

	$data = $this->get_contents($this->data);
	//echo "<pre>";
	//print_r($this->data);

	while($row = $data->fetch_assoc()){ //print_r($row);
		$row['image_url'] = $this->assets.$row['image_url'];
		$row['server_time'] = date('Y-m-d H:i:s');
		$dataset[] = $row;
	}

	echo json_encode($dataset);
}

public function subs_activity(){ 
	$data = array('subscriber_id' => $this->data['subscriber_id'],
				'content_id' => $this->data['content_id'],
				'is_read' => $this->data['is_read'] ? 1 : 0,
				'is_deleted' => $this->data['is_deleted'] ? 1 : 0,
				'is_favorite' => $this->data['is_favorite'] ? 1 : 0
				);

	$inserted = $this->insert_activity($data); 
	
	if($inserted == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	
	echo json_encode($msg);
}

public function is_read(){
	$updated = $this->update_activity_read($this->data['subscriber_id'], $this->data['content_id']);
#var_dump($updated);
	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function is_deleted(){
	$updated = $this->update_activity_deleted($this->data['subscriber_id'], $this->data['content_id']);
#var_dump($updated);
	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function is_favorite(){
	$updated = $this->update_activity_favorite($this->data['subscriber_id'], $this->data['content_id']);
#var_dump($updated);
	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function edit_profile(){

	$dataset = $this->get_password($this->data['msisdn']);

	if ($data['password'] && ($dataset['password'] != $this->data['old_password'])) {
    	$msg = array('status' => "failed", 'message' => 'Passwords does not match with your current password');
    }else{
    	unset($this->data['method']);
    	unset($this->data['old_password']);
    	$updated = $this->edit_subscriber_details($this->data);
		if($updated == 1){
			$msg = array('status' => "success");
		}else{
			$msg = array('status' => "failed");
		}
    }

	echo json_encode($msg);
}

public function optin(){
	$data = array(
	'msisdn' => $this->data['msisdn'], 
	'subscription_type' => $this->data['subscription_type'],
	'status_id' => $this->data['status_id'],
	'is_charged'=> $this->data['is_charged'],
	'next_charge' => $this->data['next_charge']);

	$inserted = $this->insert_subscriber($data);
	//var_dump($inserted);
	if($inserted){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}

	echo json_encode($msg);
}

public function status_id(){
	$updated = $this->update_status_id($this->data['msisdn'], $this->data['status_id']);

	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function check_subscriber(){
	$already_exist = $this->get_check_subscriber($this->data['msisdn']);
	//print_r($already_exist);
	if(count($already_exist)){
		$data = array('status' => "success", 
			'message' => "Record Found", 
			'subscriber' => $already_exist
			);
	}else{
		$data = array('status' => "failed", 'message' => "No Record Found");
	}
	echo json_encode($data);

}

public function push_charged(){
	$updated = $this->update_charged_date($this->data['msisdn'], $this->data['is_charged'], $this->data['next_charge']);
	//var_dump($updated);
	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function log(){
	$data = array(
		'subscriber_id' => $this->data['subscriber_id'],
		'subscription_type' => $this->data['subscription_type'],
		'status_id' => $this->data['status_id']
		);

	$id = $this->insert_subscriber_log($data);

	if($id){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function badge(){
	
	$categories = $this->get_content_categories();

	while($row = $categories->fetch_assoc()){ 
		$this->data['category_id'] = $row['category_id'];

		$data = $this->get_badge($this->data);
		
		while($row = $data->fetch_assoc()){ 
			$dataset[] = $row;
		}
	}
	echo json_encode($dataset);
}

public function subscription_type(){

	$updated= $this->update_subscription_type($this->data['msisdn'], $this->data['subscription_type']);
	if($updated == 1){
		$msg = array('status' => "success");
	}else{
		$msg = array('status' => "failed");
	}
	echo json_encode($msg);
}

public function freetrial(){
	$dataset = $this->get_subscriber($this->data['msisdn']);
	//print_r($dataset);
	$updated = $this->update_freetrial_countdown($this->data['msisdn']);
	
	if($dataset['freetrial'] == -1){
		$msg = array('status' => "failed", 'msg' => "already subscribed");
	}else{
		if($updated == 1){
			$msg = array('status' => "success");
		}else{
			$msg = array('status' => "failed");
		}
	}

	echo json_encode($msg);
}

public function subscriber_update(){

	$msisdn = $this->data['msisdn'];

	unset($this->data['method']);
	unset($this->data['msisdn']);

	$updated = $this->update_subscriber($msisdn,$this->data);

	if($updated == 1){
			$msg = array('status' => "success");
		}else{
			$msg = array('status' => "failed");
		}

	echo json_encode($msg);
}

public function version(){
	$msg = array('status' => "success", 'message' => $this->version);
	echo json_encode($msg);
}

public function session(){

	$dataset = $this->check_session($this->data['subscriber_id'],$this->data['device_id']);

	//print_r($dataset);

	if($dataset)
	{
		//if($dataset['session_active'] == 1){
			$msg = array('session' => '1');
		//}else{
		//	$msg = array('session' => '0');
		//}
	
		$this->update_session($this->data['subscriber_id']);
		$this->update_session_device($this->data['subscriber_id'],$this->data['device_id']);
		
	}else{
		$msg = array('session' => '1');

		$data = array('device_id' => $this->data['device_id'],
					'subscriber_id' => $this->data['subscriber_id']);
		
		$this->insert_session($data);
	}

	echo json_encode($msg);
}

public function user_session(){

	$dataset = $this->check_session($this->data['subscriber_id'],$this->data['device_id']);
	echo json_encode($dataset);
}

public function __destruct(){
	parent::__destruct();
}

}//eoc
header('Content-Type: application/json');

$data = array();

if($_POST){
	$data = $_POST;
}elseif($_GET){
	$data = $_GET;
}
//echo 'TEST';
//echo '<pre>'; var_dump($data);
new app($data);
