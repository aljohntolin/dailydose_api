<?php
/**
 * Created By: katherinedepadua
 * Company: Yondu Inc.
 * Department: MS - Platform
 * Date: 09/07/2015
 * Purpose: Daiy Dose API
 */
class ddose {

	public $con;

	public function __construct(){
        $this->_connect();
	}

	public function _connect(){
         $db_ip = '10.208.152.24';
         $uname = 'project_ddose';
         $password = 'DA1s31Dlly3';
         $db_name = 'Daily_Dose';

        $this->con = mysqli_connect($db_ip, $uname, $password, $db_name) 
                     or die("Failed to connect to MySQL: " . mysqli_connect_error()); 
                     //var_dump($this->con);
    }

    public function register_subscriber($data){ 
        $insert = "INSERT INTO Daily_Dose.registration
            (facebook_id, msisdn, fullname, password, email, birthday, image_url, gcm_id)
            VALUES ('" . implode("','", $data)."');";
        //echo $insert; var_dump(mysqli_error($insert));
        $this->con->query($insert);
        return $this->con->insert_id;
    }

    public function insert_initial_content($id){
        $sql = "INSERT INTO Daily_Dose.subscriber_activity 
                (subscriber_id,content_id, is_read, is_deleted, is_favorite)
                SELECT {$id}, content_id, 0, 0, 0 FROM Daily_Dose.content 
                WHERE DATE(schedule) = DATE(NOW());";
            $this->con->query($sql);
        return $this->con->affected_rows;
    }

    public function get_subscriber($msisdn){ 
        $sql = "SELECT subscriber_id,facebook_id,gcm_id,msisdn,fullname,email,birthday,image_url,registered_at,freetrial
                FROM Daily_Dose.registration WHERE msisdn = {$msisdn};"; 
        $result = $this->con->query($sql); //var_dump($res);
        $data = $result->fetch_assoc(); //var_dump($data);
        return $data;
    }
    public function get_credentials($msisdn){ 
        $sql = "SELECT subscriber_id,facebook_id,gcm_id,msisdn,fullname,password,email,birthday,image_url,registered_at,freetrial
                FROM Daily_Dose.registration WHERE msisdn = {$msisdn};"; 
        $result = $this->con->query($sql); //var_dump($res);
        $data = $result->fetch_assoc(); //var_dump($data);
        return $data;
    }
    public function get_password($msisdn){ 
        $sql = "SELECT password
                FROM Daily_Dose.registration WHERE msisdn = {$msisdn};"; 
        $result = $this->con->query($sql); //var_dump($res);
        $data = $result->fetch_assoc(); //var_dump($data);
        return $data;
    }

    public function edit_subscriber_details($data){ 
        $update = "UPDATE Daily_Dose.registration 
                SET ".implode(',', $this->update_data($data))."
                WHERE msisdn = '{$data['msisdn']}'"; 
                //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function get_mobile_login($user_id,$password){
        $sql = "SELECT * FROM Daily_Dose.registration 
                WHERE msisdn = '{$user_id}' 
                AND password = '{$password}';";
        //echo $sql;
        $result = $this->con->query($sql);
        $data = $result->fetch_assoc();
        return $data;
    }

    public function get_fb_login($facebook_id){
        $sql = "SELECT * FROM Daily_Dose.registration WHERE facebook_id = {$facebook_id};";
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function insert_subscriber($data){
        $insert = "INSERT INTO Daily_Dose.subscriber
            (msisdn, subscription_type,status_id,is_charged,next_charge)
            VALUES ('" . implode("','", $data)."');";
        //echo $insert;
        $this->con->query($insert);
        return $this->con->insert_id;
    }

    public function insert_subscriber_log($data){
        $insert = "INSERT INTO Daily_Dose.subscriber_log
            (subscriber_id, subscription_type, status_id)
            VALUES ('" . implode("','", $data)."');";
        //echo $insert;
        $this->con->query($insert);
        return $this->con->insert_id;
    }

    public function get_check_subscriber($msisdn){
       $sql = "SELECT * FROM Daily_Dose.subscriber 
                WHERE msisdn = '{$msisdn}';"; 
                #AND status_id NOT IN (2,4,5);"; 
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function insert_verification_code($data){
        $insert = "INSERT INTO Daily_Dose.verification_code
            (msisdn, verification_code)
            VALUES ('" . implode("','", $data)."');";
        //echo $insert;
        $this->con->query($insert);
    }

    public function check_verification_code($msisdn, $code){
       $sql = "SELECT * FROM Daily_Dose.verification_code 
                WHERE msisdn = '{$msisdn}' AND BINARY verification_code = '{$code}';"; 
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function check_msisdn_verification($msisdn){
      $sql = "SELECT * FROM Daily_Dose.verification_code 
                WHERE msisdn = '{$msisdn}';"; 
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function update_verification_code($msisdn, $vcode){
        $update = "UPDATE Daily_Dose.verification_code SET verification_code = '$vcode', verified_at = NOW()
                WHERE msisdn = '{$msisdn}'";
        $this->con->query($update);
    }

    public function update_verified_at($msisdn){
        $update = "UPDATE Daily_Dose.verification_code SET verified_at = NOW()
                WHERE msisdn = '{$msisdn}'";
        $this->con->query($update);
    }

    public function update_password($msisdn, $new_password){
        $update = "UPDATE Daily_Dose.registration SET password = '$new_password'
                WHERE msisdn = '{$msisdn}'";
        $this->con->query($update);
    }

    public function update_gcm_id($msisdn, $gcm){
        $update = "UPDATE Daily_Dose.registration SET gcm_id = '{$gcm}'
                WHERE msisdn = '{$msisdn}'";
        $this->con->query($update);
    }

    public function get_content_categories(){
        $sql = "SELECT * FROM Daily_Dose.content_category ORDER BY category_position;"; 
        $result = $this->con->query($sql);  
        return $result;
    }

    public function get_contents($data){
        $sql = "SELECT c.content_id, title, subcategory, cs.image_url, content, content_type, schedule, is_read
                FROM Daily_Dose.content_setting cs
                JOIN Daily_Dose.content c
                ON cs.content_id = c.content_id
                JOIN Daily_Dose.subscriber_activity sa
                ON sa.content_id = c.content_id ";

                if($data['freetrial'] == -1){
                    /*$sql .= "JOIN Daily_Dose.subscriber s
                    ON s.subscriber_id = sa.subscriber_id ";*/
                    $sql .= "JOIN Daily_Dose.registration r
                            ON r.subscriber_id = sa.subscriber_id
                            JOIN Daily_Dose.subscriber s
                            ON s.msisdn = r.msisdn ";
                }

        $sql .= "WHERE is_content_active = 1
                AND category_id = {$data['category_id']}
                AND sa.subscriber_id = {$data['subscriber_id']}
                AND is_deleted = 0 ";

                if($data['category_id'] == 4){
                    $sql .= " AND DATE(schedule) = DATE(NOW()) ";
                }else{
                    $sql .= " AND schedule <= NOW() ";
                }

                if($data['freetrial'] == 0 ){
                    $sql .= " AND content_type = 0 ";
                }elseif($data['freetrial'] == -1){
                    $sql .= " AND is_charged = 1 ";
                }

        $sql .= " ORDER BY DATE(schedule) DESC
                 LIMIT {$data['limit']}
                OFFSET {$data['offset']};"; 

        #echo $sql;

        $result = $this->con->query($sql);  
        return $result;
    }

    public function insert_activity($data){
      $insert = "INSERT INTO Daily_Dose.subscriber_activity
            (subscriber_id, content_id, is_read, is_deleted, is_favorite)
            VALUES ('" . implode("','", $data)."');";
        $this->con->query($insert); 
        return $this->con->affected_rows;
    }

    public function update_activity_read($subscriber_id, $content_id){
        $update = "UPDATE Daily_Dose.subscriber_activity SET is_read = '1'
                WHERE subscriber_id = '{$subscriber_id}'
                AND content_id = '{$content_id}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_activity_deleted($subscriber_id, $content_id){
        $update = "UPDATE Daily_Dose.subscriber_activity SET is_deleted = '1'
                WHERE subscriber_id = '{$subscriber_id}'
                AND content_id = '{$content_id}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_activity_favorite($subscriber_id, $content_id, $value){
        $update = "UPDATE Daily_Dose.subscriber_activity SET is_favorite = '{$value}'
                WHERE subscriber_id = '{$subscriber_id}'
                AND content_id = '{$content_id}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_charged_date($msisdn, $is_charged, $next_charge){
        $update = "UPDATE Daily_Dose.subscriber SET is_charged = '{$is_charged}', next_charge = '{$next_charge}'
                WHERE msisdn = '{$msisdn}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_subscription_type($msisdn, $subscription_type){
        $update = "UPDATE Daily_Dose.subscriber SET subscription_type = '{$subscription_type}'
                WHERE msisdn = '{$msisdn}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_freetrial_countdown($msisdn){
        $update = "UPDATE Daily_Dose.registration SET freetrial = '-1'
                WHERE msisdn = '{$msisdn}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_status_id($msisdn, $status_id){
        $update = "UPDATE Daily_Dose.subscriber SET status_id = '{$status_id}'
                WHERE msisdn = '{$msisdn}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function get_badge($data){
        $sql = "SELECT category_id,SUM(if(is_read=0,1,0)) as badge
                FROM Daily_Dose.content_setting cs
                JOIN Daily_Dose.content c
                ON cs.content_id = c.content_id
                JOIN Daily_Dose.subscriber_activity sa
                ON sa.content_id = c.content_id ";

                if($data['freetrial'] == -1){
                    $sql .= "JOIN Daily_Dose.subscriber s
                    ON s.subscriber_id = sa.subscriber_id ";
                }

        $sql .= "WHERE is_content_active = 1
                AND category_id = {$data['category_id']}
                AND sa.subscriber_id = {$data['subscriber_id']}
                AND is_deleted = 0 ";

                if($data['category_id'] == 4){
                    $sql .= " AND DATE(schedule) = DATE(NOW()) ";
                }else{
                    #$sql .= " AND DATE(schedule) <= DATE(NOW()) ";
                    $sql .= " AND schedule <= NOW() ";
                }

                if($data['freetrial'] == 0 ){
                    $sql .= " AND content_type = 0 ";
                }elseif($data['freetrial'] == -1){
                    $sql .= " AND is_charged = 1 ";
                }

        //$sql .= " ORDER BY cate"; 
                //echo $sql;
        $result = $this->con->query($sql);  
        return $result;
    }

    public function update_registration_details($data){
        $update = "UPDATE Daily_Dose.registration 
            SET image_url = '{$data['image_url']}'
            WHERE msisdn = '{$data['msisdn']}';"; //echo $update;

        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_subscriber($msisdn,$data){

        $update = "UPDATE Daily_Dose.subscriber 
            SET ".implode(',', $this->update_data($data))."
                WHERE msisdn = '{$msisdn}';"; 

        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function check_session($subscriber_id,$device_id){
        $sql = "SELECT * FROM Daily_Dose.user_session 
                WHERE subscriber_id = '{$subscriber_id}' AND device_id = '{$device_id}';"; 
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function insert_session($data){
        $insert = "INSERT INTO Daily_Dose.user_session
            (device_id,subscriber_id)
            VALUES ('" . implode("','", $data)."');";
        //echo $insert;
        $this->con->query($insert);
        return $this->con->insert_id;
    }

    public function update_session($subscriber_id){
        $update = "UPDATE Daily_Dose.user_session SET session_active = '0'
                WHERE subscriber_id = '{$subscriber_id}'
                "; //echo $update;
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    public function update_session_device($subscriber_id,$device_id){
        $update = "UPDATE Daily_Dose.user_session SET session_active = '1'
                WHERE subscriber_id = '{$subscriber_id}'  AND device_id = '{$device_id}';"; 
        $this->con->query($update);
        return $this->con->affected_rows;
    }

    /*
    *
    */
    public function update_data($data){
        foreach($data as $field_name => $field_value) {
           $sql_str[] = "{$field_name} = '{$field_value}'";
        }
        return $sql_str;
    }

    public function send_sms($data){

    //Erika 2630
    //$url = 'http://10.178.7.44/dmp/api/rt/sms/2630.php?msisdn='.$data['msisdn'].'&message='.$data['message'];

    //Sheena 2910
    $url = 'http://10.178.20.221/dmp/api/rt/sms/index.php?msisdn='.$data['msisdn'].'&message='.$data['message'];

      //echo $url;
         $ch = curl_init();
          curl_setopt($ch,CURLOPT_URL,$url);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
          curl_setopt($ch,CURLOPT_HEADER, false);
          $output=curl_exec($ch);
          curl_close($ch);
          //echo $output;
    }

    public function get_gcms(){
        $sql = "SELECT gcm_id FROM Daily_Dose.registration;";
        $result = $this->con->query($sql);  
        return $result;
    }

    public function get_content_today($category_id){
        $sql = "SELECT cs.title, c.* FROM Daily_Dose.content c
                JOIN Daily_Dose.content_setting cs
                ON c.content_id = cs.content_id
                WHERE category_id = {$category_id} 
                AND is_content_active = 1
                AND DATE(schedule) = DATE(NOW());"; 
        $result = $this->con->query($sql); 
        $data = $result->fetch_assoc(); 
        return $data;
    }

    public function randomString($length, $type = '') {
          // Select which type of characters you want in your random string
          switch($type) {
            case 'num':
              // Use only numbers
              $salt = '1234567890';
              break;
            case 'lower':
              // Use only lowercase letters
              $salt = 'abcdefghijklmnopqrstuvwxyz';
              break;
            case 'upper':
              // Use only uppercase letters
              $salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
              break;
            case 'alpha':
                $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            default:
              // Use uppercase, lowercase, numbers, and symbols
              $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
              break;
          }
          $rand = '';
          $i = 0;
          while ($i < $length) { // Loop until you have met the length
            $num = rand() % strlen($salt);
            $tmp = substr($salt, $num, 1);
            $rand = $rand . $tmp;
            $i++;
          }
          return $rand; // Return the random string
    }

    public function _disconnect(){
        mysqli_close($this->con);
    }

	public function __destruct(){
        $this->_disconnect();
	}
}


