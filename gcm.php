<?php
require 'ddose.class.php';
$ddose = new ddose;
//------------------------------
// Payload data you want to send 
// to Android device (will be
// accessible via intent extras)
//------------------------------
$category_id = $argv[1];
$content = $ddose->get_content_today($category_id);
//print_r($content);

if($category_id == 4){
    $content['content'] = "Find out your sign's horoscope today!";
    $content['content_type'] = 1;
}

$data = array( 'message' => $content['content_type'].'|'.$content['title'] );

//------------------------------
// The recipient registration IDs
// that will receive the push
// (Should be stored in your DB)
// 
// Read about it here:
// http://developer.android.com/google/gcm/
//------------------------------

$gcm_id = $ddose->get_gcms();
while($row = $gcm_id->fetch_assoc()){
        $ids[] = $row['gcm_id'];
    }
//echo "<pre>";
//print_r($ids);
//print_r($data);

echo "Push Time - ".date('Y-m-d H:i:s')."\n";
//$ids = array('APA91bGOzXCeo2Ro_Ya3Am3Y71a-s5c0f9Sg3OCCFVhcFMg2Pj4Qd4e2m4lkpojYqE6AoEkJsQCqsuE-wPBHKTJcgeZmf28fZjb_sjilwCtDgSqygnX1H7r8sFC3vIECyBcKUR7JfpMb');

//------------------------------
// Call our custom GCM function
//------------------------------

sendGoogleCloudMessage(  $data, $ids );

//------------------------------
// Define custom GCM function
//------------------------------

function sendGoogleCloudMessage( $data, $ids )
{
    //------------------------------
    // Replace with real GCM API 
    // key from Google APIs Console
    // 
    // https://code.google.com/apis/console/
    //------------------------------

    $apiKey = 'AIzaSyBN19y_R0Bg4-BfFCKwHg2tFd5Mfa-m-ik'; #'AIzaSyBKHXqCM5S_ivsbrI5sgHW8Lulxl8e0h_s';

    //------------------------------
    // Define URL to GCM endpoint
    //------------------------------

    $url = 'https://android.googleapis.com/gcm/send';

    //------------------------------
    // Set GCM post variables
    // (Device IDs and push payload)
    //------------------------------

    $post = array(
                    'registration_ids'  => $ids,
                    'data'              => $data,
                    );

    //------------------------------
    // Set CURL request headers
    // (Authentication and type)
    //------------------------------

    $headers = array( 
                        'Authorization: key=' . $apiKey,
                        'Content-Type: application/json'
                    );

    //------------------------------
    // Initialize curl handle
    //------------------------------

    $ch = curl_init();

    //------------------------------
    // Set URL to GCM endpoint
    //------------------------------

    curl_setopt( $ch, CURLOPT_URL, $url );

    //------------------------------
    // Set request method to POST
    //------------------------------

    curl_setopt( $ch, CURLOPT_POST, true );

    //------------------------------
    // Set our custom headers
    //------------------------------

    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

    //------------------------------
    // Get the response back as 
    // string instead of printing it
    //------------------------------

    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    //------------------------------
    // Set post data as JSON
    //------------------------------

    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

    //------------------------------
    // Actually send the push!
    //------------------------------

    $result = curl_exec( $ch );

    //------------------------------
    // Error? Display it!
    //------------------------------

    if ( curl_errno( $ch ) )
    {
        echo 'GCM error: ' . curl_error( $ch );
    }

    //------------------------------
    // Close curl handle
    //------------------------------

    curl_close( $ch );

    //------------------------------
    // Debug GCM response
    //------------------------------

    echo $result;
}
